using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    [HideInInspector] public AudioSource Source;

    [SerializeField] private static AudioClip _clip;

    [SerializeField] private string _name;

    [Range(0f, 1f)]
    [SerializeField] private float _volume;
    [Range(.1f, 3f)]
    [SerializeField] private float _pitch;
    [SerializeField] private bool _loop;
   

    public AudioClip Clip = _clip;

    public string Name => _name;

    public float Volume => _volume;

    public float Pitch => _pitch;

    public bool Loop => _loop;
}
