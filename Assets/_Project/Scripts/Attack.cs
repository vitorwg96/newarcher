using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField] private AttackConfigSO _attacker;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out CharacterStats damageable))
        {
            damageable.TakeDamage(_attacker.AttackStrength);
        }
    }
}