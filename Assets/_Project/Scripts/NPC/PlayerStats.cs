using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PlayerStats : CharacterStats
{
    public event UnityAction IsDead;
    public event UnityAction<int> TookDamage;

    private float _maxMoveSpeed;

    private void Start()
    {
        _maxMoveSpeed = _stats.MoveSpeed;
        ResetHealth();
    }

    public override void TakeDamage(int enemyDamage)
    {
        base.TakeDamage(enemyDamage);
        TookDamage?.Invoke(_currentHealth);
        AudioManager.instance.Play("PlayerTakeDamage");
    }

    public override void OnDie()
    {
        base.OnDie();
        IsDead?.Invoke();
        Debug.Log("Player is dead");
    }

    public void RestoreMoveSpeed(float slowTime)
    {
        StartCoroutine(IsRestoringSpeed());

        IEnumerator IsRestoringSpeed()
        {
            yield return new WaitForSeconds(slowTime);
            _stats.MoveSpeed = _maxMoveSpeed;
        }
    }
}
