using UnityEngine;
using UnityEngine.Events;

public class CharacterStats : MonoBehaviour, ITakeDamage
{
    [SerializeField] protected StatsConfigSO _stats;

    protected int _currentHealth;

    private void Start()
    {
        _currentHealth = _stats.Health;
    }

    public virtual void TakeDamage(int enemyDamage)
    {
        _currentHealth -= enemyDamage;

        //Debug.Log(gameObject.name + " takes " + enemyDamage + " damage.");

        if (_currentHealth <= 0)
        {
            OnDie();
        }
    }

    public virtual void OnDie()
    {
        Debug.Log(gameObject.name + " isDead");
    }

    public void ResetHealth()
    {
        _currentHealth = _stats.Health;
    }

    public StatsConfigSO GetStats()
    {
        return _stats;
    }
}
