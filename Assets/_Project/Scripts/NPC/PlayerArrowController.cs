using UnityEngine;


public class PlayerArrowController : MonoBehaviour
{
    private InputReader _inputReader;
    private SystemProjectileShoot _systemProjectileShoot;
    private ArrowType _actualArrowType;

    public void EquipArrowType(ArrowType arrowType)
    {
        _actualArrowType = arrowType;
        Debug.Log("ArrowType Equiped: " + _actualArrowType);
        _systemProjectileShoot.SetIndexProjectile(_actualArrowType);
    }

    private void Awake()
    {
        _inputReader = GetComponent<InputReader>();
        _systemProjectileShoot = GetComponent<SystemProjectileShoot>();
    }

    private void OnEnable()
    {
        SubscribeEvents();
    }
    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        _inputReader.EquipNormalArrowEvent += EquipArrowType;
        _inputReader.EquipPiercingArrowEvent += EquipArrowType;
        _inputReader.EquipExplodeArrowEvent += EquipArrowType;
    }

    private void UnsubscribeEvents()
    {
        _inputReader.EquipNormalArrowEvent -= EquipArrowType;
        _inputReader.EquipPiercingArrowEvent -= EquipArrowType;
        _inputReader.EquipExplodeArrowEvent -= EquipArrowType;
    }
}
