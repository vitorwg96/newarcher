using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour
{
    private NavMeshAgent _agent;
    private GameObject _target;
    private PlayerStats _targetStats;
    private Animator _animator;
    private Rigidbody _enemyRb;
    private SystemProjectileShoot _projectileSystem;
    private EnemyStats _enemyStats;

    private float _distance;
    private float _lerpRotation = 5f;

    private void Awake()
    {
        _target = Target.instance.GetPlayerTarget();
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _enemyRb = GetComponent<Rigidbody>();
        _projectileSystem = GetComponent<SystemProjectileShoot>();
        _enemyStats = GetComponent<EnemyStats>();
    }

    private void Start()
    {
        _targetStats = _target.GetComponent<PlayerStats>();
    }

    private void OnEnable()
    {
        _animator.SetBool("IsMoving", true);
        _agent.speed = _enemyStats.GetStats().MoveSpeed;
    }

    private void OnDisable()
    {
        _animator.SetBool("IsMoving", false);
    }

    private void Update()
    {
        OnEnemyMove();
    }

    public void DoProjectileAttack(GameObject gameObject)
    {
        _projectileSystem.IsShooting();
        _agent.isStopped = false;
    }

    public void PlayAttackSound(string path)
    {
        AudioManager.instance.Play(path);
    }

    public void OnRangedAttackTrigger(bool entered, GameObject who)
    {
        _agent.isStopped = true;
        FaceTarget();
        _animator.SetTrigger("RangedAttack");
    }

    private void OnEnemyMove()
    {
        _distance = Vector3.Distance(transform.position, _target.transform.position);

        if (_distance <= (_agent.stoppingDistance + 0.3f))
        {
            _animator.SetTrigger("Attack");
            FaceTarget();
        }
        else
        {
            _agent.SetDestination(_target.transform.position);
        }
    }
    private void FaceTarget()
    {
        Vector3 direction = (_target.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0.0f, direction.z));
        transform.rotation = Quaternion.Slerp(lookRotation, transform.rotation, Time.deltaTime * _lerpRotation);
    }

}
