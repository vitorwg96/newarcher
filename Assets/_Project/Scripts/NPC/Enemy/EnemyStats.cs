using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class EnemyStats : CharacterStats
{
    public static event UnityAction OnEnableCount;
    public static event UnityAction IsDead;

    private NavMeshAgent _agent;
    private Animator _animator;
    private Collider _collider;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        _collider.enabled = true;
    }

    public override void TakeDamage(int enemyDamage)
    {
        _animator.SetTrigger("TakeDamage");
        AudioManager.instance.Play("SpiderTakeDamage");
        base.TakeDamage(enemyDamage);
    }

    public override void OnDie()
    {
        _agent.speed = 0;
        _animator.SetTrigger("IsDead");
        AudioManager.instance.Play("SpiderDeath");
        _collider.enabled = false;
        IsDead?.Invoke();
        StartCoroutine(ReturnObject());
    }

    IEnumerator ReturnObject()
    {
        yield return new WaitForSeconds(3f);
        PoolerObjectAdvanced.ReturnGameObject(gameObject); 
    }
}
