using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class BoolEvent : UnityEvent<bool, GameObject> { }

public class ZoneTriggerController : MonoBehaviour
{
	[SerializeField] private BoolEvent _enterZone = default;

	private void OnTriggerEnter(Collider other)
	{
		if (other.TryGetComponent(out PlayerStats playerStats))
		{
			//Debug.Log("Other Obj " + other.gameObject.name + " and playerStats Obj " + playerStats.gameObject.name);
			_enterZone.Invoke(true, other.gameObject);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.TryGetComponent(out PlayerStats playerStats))
		{
			_enterZone.Invoke(false, other.gameObject);
		}
	}
}
