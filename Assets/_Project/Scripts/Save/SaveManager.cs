using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    [SerializeField] private InventorySO _inventory;

    public void Save()
    {
        InventorySO inventory = _inventory;
        int coinAmount = _inventory.CoinAmount;
        int normalArrowAmount = _inventory.NormalArrowAmount;
        int piercingArrowAmount = _inventory.PiercingArrowAmount;
        int explodeArrowAmount = _inventory.ExplodeArrowAmount;
        int healthPotionAmount = _inventory.HealthPotionAmount;

        SaveObject saveObject = new SaveObject
        {
            inventory = inventory,
            coinAmount = coinAmount,
            normalArrowAmount = normalArrowAmount,
            piercingArrowAmount = piercingArrowAmount,
            explodeArrowAmount = explodeArrowAmount,
            healthPotionAmount = healthPotionAmount
        };
        string json = JsonUtility.ToJson(saveObject);
        SaveSystem.Save(json);

        Debug.Log("Saved!");
    }

    public void Load()
    {
        string saveString = SaveSystem.Load();
        if (saveString != null)
        {

            SaveObject saveObject = JsonUtility.FromJson<SaveObject>(saveString);

            _inventory = saveObject.inventory;

            _inventory.CoinAmount = saveObject.coinAmount;
            _inventory.NormalArrowAmount = saveObject.normalArrowAmount;
            _inventory.PiercingArrowAmount = saveObject.piercingArrowAmount;
            _inventory.ExplodeArrowAmount = saveObject.explodeArrowAmount;
            _inventory.HealthPotionAmount = saveObject.healthPotionAmount;

            Debug.Log("Loaded: " + saveString);
        }
        else
        {
            Debug.Log("No save");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            Save();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
    }
    private class SaveObject
    {
        public InventorySO inventory;
        public int coinAmount;
        public int normalArrowAmount;
        public int piercingArrowAmount;
        public int explodeArrowAmount;
        public int healthPotionAmount;
    }
}
