using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using System;

public class InputReader : MonoBehaviour
{
    private PlayerInputActions _inputActions;
    
    // Gameplay
    public event UnityAction<Vector2> OnMoveEvent;
    public event UnityAction<Vector2> OnLookEvent;
    public event UnityAction OnShootEvent;
    public event UnityAction<ArrowType> EquipNormalArrowEvent;
    public event UnityAction<ArrowType> EquipPiercingArrowEvent;
    public event UnityAction<ArrowType> EquipExplodeArrowEvent;

    // Menus
    public event UnityAction MenuPauseEvent;
    public event UnityAction MenuClickButtonEvent;

    private void Awake()
    {
        _inputActions = new PlayerInputActions();
        SubscribeEvents();
    }
    private void OnEnable()
    {
        _inputActions.Player.Enable();
        _inputActions.UI.Enable();
    }

    private void OnDisable()
    {
        _inputActions.Player.Disable();
        _inputActions.UI.Disable();
    }

    private void OnSelectExplosiveArrow(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            EquipExplodeArrowEvent?.Invoke(ArrowType.ExplodeArrow);
        }
    }

    private void OnSelectPiercingArrow(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            EquipPiercingArrowEvent?.Invoke(ArrowType.PiercingArrow);
        }
    }

    private void OnSelectNormalArrow(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            EquipNormalArrowEvent?.Invoke(ArrowType.NormalArrow);
        }
    }

    private void OnShoot(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            OnShootEvent?.Invoke();
        }
    }

    private void OnLook(InputAction.CallbackContext context)
    {
        Vector2 lookInput = context.ReadValue<Vector2>();
        OnLookEvent?.Invoke(lookInput);
    }

    private void OnMove(InputAction.CallbackContext context)
    {
        Vector2 moveInput = context.ReadValue<Vector2>();
        OnMoveEvent?.Invoke(moveInput);
    }

    private void OnClick(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            MenuClickButtonEvent?.Invoke();
        }
    }

    private void OnPause(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            MenuPauseEvent?.Invoke();
        }
    }

    private void SubscribeEvents()
    {
        // Gameplay Input
        _inputActions.Player.Movement.performed += OnMove;
        _inputActions.Player.Look.performed += OnLook;
        _inputActions.Player.Shoot.performed += OnShoot;
        _inputActions.Player.NormalArrow.performed += OnSelectNormalArrow;
        _inputActions.Player.PiercingArrow.performed += OnSelectPiercingArrow;
        _inputActions.Player.ExplosiveArrow.performed += OnSelectExplosiveArrow;

        // UI Input
        _inputActions.UI.Pause.performed += OnPause;
        _inputActions.UI.LeftClick.performed += OnClick;
    }
}
