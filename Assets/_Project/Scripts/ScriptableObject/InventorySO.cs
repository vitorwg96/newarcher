using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "DataSave/Inventory")]
public class InventorySO : ScriptableObject
{
    [SerializeField] private int _coinAmount;
    [SerializeField] private int _normalArrowAmount;
    [SerializeField] private int _piercingArrowAmount;
    [SerializeField] private int _explodeArrowAmount;
    [SerializeField] private int _healthPotionAmount;

    public int HealthPotionAmount
    {
        get => _healthPotionAmount;
        set => _healthPotionAmount = value;
    }

    public int ExplodeArrowAmount
    {
        get => _explodeArrowAmount;
        set => _explodeArrowAmount = value;
    }

    public int PiercingArrowAmount
    {
        get => _piercingArrowAmount;
        set => _piercingArrowAmount = value;
    }

    public int NormalArrowAmount
    {
        get => _normalArrowAmount;
        set => _normalArrowAmount = value;
    }

    public int CoinAmount
    {
        get => _coinAmount;
        set => _coinAmount = value;
    }

    public int GetItemAmountType(ItemShopType itemType)
    {
        switch (itemType)
        {
            case ItemShopType.NormalArrow:
                return NormalArrowAmount;
            case ItemShopType.PiercingArrow:
                return PiercingArrowAmount;
            case ItemShopType.ExplodeArrow:
                return ExplodeArrowAmount;
            case ItemShopType.HealthPotion:
                return HealthPotionAmount;
            default:
                Debug.Log("Didn't find any amount with this type");
                return 0;
        }
    }

    public void SetItemAmountType(int amount, bool incriese,  ItemShopType itemType)
    {
        switch (itemType)
        {
            case ItemShopType.NormalArrow:
                if (incriese)
                {
                    _normalArrowAmount += amount;
                }
                else
                {
                    _normalArrowAmount -= amount;
                }
                break;
            case ItemShopType.PiercingArrow:
                if (incriese)
                {
                    _piercingArrowAmount += amount;
                }
                else
                {
                    _piercingArrowAmount -= amount;
                }
                break;
            case ItemShopType.ExplodeArrow:
                if (incriese)
                {
                    _explodeArrowAmount += amount;
                }
                else
                {
                    _explodeArrowAmount -= amount;
                }
                break;
            case ItemShopType.HealthPotion:
                if (incriese)
                {
                    _healthPotionAmount += amount;
                }
                else
                {
                    _healthPotionAmount -= amount;
                }
                break;
            default:
                Debug.Log("Didn't find any amount with this type");
                break;
        }
    }
}
