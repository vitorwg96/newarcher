using UnityEngine;

[CreateAssetMenu(fileName = "ItemShop", menuName = "ItemShopConfig/ Item Config")]
public class ItemShopConfigSO : ScriptableObject
{
    [SerializeField] private Sprite _icon;
    [SerializeField] private ItemShopType _itemType;
    [SerializeField] private int _buyPrice;
    [SerializeField] private int _sellPrice;

    public Sprite Icon => _icon;
    public ItemShopType Type => _itemType;
    public int BuyPrice
    {
        get => _buyPrice;
        set => _buyPrice = value;
    }
    public int SellPrice
    {
        get => _sellPrice;
        set => _sellPrice = value;
    }

}
