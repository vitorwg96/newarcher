using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] private GameObject _player;

    public static Target instance { get; private set; }

    private void Awake()
    {
        instance = this;
    }

    public GameObject GetPlayerTarget()
    {
        return _player;
    }

}
