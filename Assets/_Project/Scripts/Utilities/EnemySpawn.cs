using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemySpawn : MonoBehaviour
{
    public event UnityAction<int> EnemyCountEvent;
    public event UnityAction<int> WaveCountEvent;
     

    [SerializeField] private float _radius = 5.0f;
    [SerializeField] private Transform[] _spawnPoint;

    [Header("Pooler Settings")]
    [SerializeField] private List<GameObject> _enemyPrefabsList = new List<GameObject>();
    
    private int _waveNumber = 1;
    private int _enemyCount = 0;

    public int EnemyCount => _enemyCount;

    private void OnEnable()
    {
        EnemyStats.OnEnableCount += EnemyStats_OnEnableCount;
        EnemyStats.IsDead += EnemyStats_IsDead;

        GameManager.instance.OnLoadReady += OnSpawnEnemy;
    }

    private void OnDisable()
    {
        GameManager.instance.OnLoadReady -= OnSpawnEnemy;
    }

    private void OnSpawnEnemy()
    {
        SpawnEnemyWave(_waveNumber);
    }

    private void EnemyStats_IsDead()
    {
        _enemyCount--;

        if (_enemyCount == 0)
        {
            SpawnEnemyWave(_waveNumber);
        }
    }

    private void EnemyStats_OnEnableCount()
    {
        _enemyCount++;
    }

    private void SpawnEnemyWave(int enemiesToSpawn)
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            int randomEnemySpawn = Random.Range(0, _enemyPrefabsList.Count);
            GameObject newObj = PoolerObjectAdvanced.GetObject(_enemyPrefabsList[randomEnemySpawn]);
            newObj.GetComponent<CharacterStats>().ResetHealth();
            newObj.transform.position = GenerateRandomSpawn();
            newObj.SetActive(true);
            _enemyCount++;
            EnemyCountEvent?.Invoke(_enemyCount);
        }
        WaveCountEvent?.Invoke(_waveNumber);
        _waveNumber++;
    }

    private Vector3 GenerateRandomSpawn()
    {
        int i = Random.Range(0, _spawnPoint.Length);
        Vector2 randomPointCircle = Random.insideUnitCircle * _radius;
        Vector3 point3D = new Vector3(randomPointCircle.x, _spawnPoint[i].position.y, randomPointCircle.y);

        return _spawnPoint[i].position + point3D;
    }
}