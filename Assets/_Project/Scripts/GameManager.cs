using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public event UnityAction OnLoadReady;

    [SerializeField] private GameObject _loadingScreen;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of ScoreManager found!", gameObject);
            return;
        }

        instance = this;

        LoadScene(SceneIndexes.MAIN_MENU);
        //SceneManager.LoadSceneAsync((int)SceneIndexes.MAIN_MENU, LoadSceneMode.Additive);
    }

    public void LoadScene(SceneIndexes loadScene)
    {
        _loadingScreen.SetActive(true);
        StartCoroutine(LoadSceneAddictive(loadScene));
    }

    public void UnloadScene(SceneIndexes unloadScene)
    {
        _loadingScreen.SetActive(true);
        AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync((int)unloadScene);
    }

    public IEnumerator LoadSceneAddictive(SceneIndexes loadScene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync((int)loadScene, LoadSceneMode.Additive);
        asyncLoad.allowSceneActivation = false;

        while (!asyncLoad.isDone)
        {
            if (asyncLoad.progress >= 0.9f)
            {
                asyncLoad.allowSceneActivation = true;

            }
            yield return null;
        }

        if (asyncLoad.isDone)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int)loadScene));
            asyncLoad.completed += AsyncLoadCompleted;
            Debug.Log("Loading " + loadScene + " is Done");  
        }
    }

    private void AsyncLoadCompleted(AsyncOperation obj)
    {
        Debug.Log("Raise the complete load scene event");
        _loadingScreen.SetActive(false);
        OnLoadReady?.Invoke();
        //obj.completed -= AsyncLoadCompleted;
    }

}
