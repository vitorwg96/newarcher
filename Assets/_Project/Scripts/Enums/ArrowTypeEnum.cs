public enum ArrowType
{
    NormalArrow,
    PiercingArrow,
    ExplodeArrow
}