public enum SceneIndexes
{
    MANAGER = 0,
    MAIN_MENU = 1,
    MAIN = 2,
    MAP_NOCOLLIDER = 3,
    MAP_WCOLLIDER = 4
}