using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpiderRangeShoot : SystemProjectileShoot
{
    private Animator _animator;
    private NavMeshAgent _agent;

    private void Awake()
    {
        _animator = GetComponentInParent<Animator>();
        _agent = GetComponentInParent<NavMeshAgent>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out PlayerStats playerStats))
        {
            _agent.isStopped = true;
            _animator.SetTrigger("RangedAttack");
            StartCoroutine(IsShoting());

            IEnumerator IsShoting()
            {
                IsShooting();
                
                yield return new WaitForSeconds(1.5f);
                _agent.isStopped = false;
            }

        }
    }
}
