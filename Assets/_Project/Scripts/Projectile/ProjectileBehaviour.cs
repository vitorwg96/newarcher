using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ProjectileBehaviour : MonoBehaviour
{
    [SerializeField] GameObject _centerOfMass = default;
    protected Rigidbody _projectileRb;

    private float _forcePower = 50.0f;

    private void Awake()
    {
        _projectileRb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _projectileRb.velocity = Vector3.forward;
    }

    public void AddForce()
    {
        _projectileRb.AddForce(transform.forward * _forcePower, ForceMode.Impulse);
    }


    private void OnDisable()
    {
        _projectileRb.velocity = Vector3.zero;
    }
}
