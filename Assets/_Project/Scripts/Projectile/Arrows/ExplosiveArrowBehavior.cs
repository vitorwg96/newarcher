using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveArrowBehavior : NormalArrowBehavior
{
    [SerializeField] private float _explosionRadius;
    [SerializeField] private float _explosionPower;

    public override void ArrowMecanic(Collider other)
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, _explosionRadius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
            {
                Debug.Log(hit.gameObject.name);
                rb.AddExplosionForce(_explosionPower, explosionPos, _explosionRadius, 3.0F);
            }
        }
    }
}
