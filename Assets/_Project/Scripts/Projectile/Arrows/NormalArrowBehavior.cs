using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalArrowBehavior : ProjectileBehaviour
{
    protected BoxCollider[] _collider;
    private void Start()
    {
        InitializeParameters();
    }

    protected virtual void InitializeParameters()
    {
        _collider = GetComponentsInChildren<BoxCollider>();
    }

    public virtual void ArrowMecanic(Collider other)
    {
        if (other.TryGetComponent(out EnemyStats enemyStats))
        {
            PoolerObjectAdvanced.ReturnGameObject(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ArrowMecanic(other);

        if (other.gameObject.isStatic)
        {
            _projectileRb.isKinematic = true;
            foreach (BoxCollider boxcol in _collider)
            {
                boxcol.enabled = false;
            }
            StartCoroutine(DespawnArrow());
        }

        IEnumerator DespawnArrow()
        {
            yield return new WaitForSeconds(2);
            PoolerObjectAdvanced.ReturnGameObject(gameObject);
            _projectileRb.isKinematic = false;
            foreach (BoxCollider boxcol in _collider)
            {
                boxcol.enabled = false;
            }
        }
    }

}
