using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiercingArrowBehaviour : NormalArrowBehavior
{
    // number of enemies the arrow can hit
    [SerializeField] private int _enemyCountStart;

    private int _enemyCountPiercing;

    private void Start()
    {
        InitializeParameters();
    }

    protected override void InitializeParameters()
    {
        base.InitializeParameters();
        ResetCountPiercing();
    }


    public override void ArrowMecanic(Collider other)
    {
        if (other.TryGetComponent(out EnemyStats enemyStats))
        {
            //Debug.Log(gameObject.name + " Colliding with: " + other.gameObject.name + ", count: " + _enemyCountPiercing);
            _enemyCountPiercing--;

            if (_enemyCountPiercing == 0)
            {
                PoolerObjectAdvanced.ReturnGameObject(gameObject);
                ResetCountPiercing();
                //Debug.Log("Reseting Count Piercing: " + _enemyCountPiercing);
            }
            
        }
    }

    private void ResetCountPiercing()
    {
        // multiply by 2, becouse have two componet collider
        _enemyCountPiercing = _enemyCountStart * 2;
    }
}
