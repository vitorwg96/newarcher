using UnityEngine;

public class CobwebBehaviour : ProjectileBehaviour
{
    [SerializeField] private float _slowTime = 5f;
    [SerializeField] private float _changeSpeed = 1f;

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.TryGetComponent(out PlayerStats playerStats))
        {
            playerStats.GetStats().MoveSpeed = _changeSpeed;
            playerStats.RestoreMoveSpeed(_slowTime);
            PoolerObjectAdvanced.ReturnGameObject(gameObject);
        }
    }

}
