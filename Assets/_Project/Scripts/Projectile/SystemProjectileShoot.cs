using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SystemProjectileShoot : MonoBehaviour
{
    public event UnityAction ShootedPiercingArrow;
    public event UnityAction ShootedExplodeArrow;

    [SerializeField] private AttackConfigSO _attacker;
    [SerializeField] private GameObject _viewPoint;
    [SerializeField] private GameObject _shootPoint;
    [SerializeField] private InventorySO _resource;
    [SerializeField] private List<GameObject> _projectilePrefab = new List<GameObject>();

    private GameObject _instanciatePrefab;
    private CharacterStats _characterStats;
    protected float _attackCountdown = 0f;
    private int _indexProjectile;
    private ArrowType _actualType;


    private void Awake()
    {
        _characterStats = GetComponent<CharacterStats>();
    }

    private void Update()
    {
        _attackCountdown -= Time.deltaTime;
    }

    public void SetIndexProjectile(ArrowType arrowType)
    {
        _actualType = arrowType;

        switch (_actualType)
        {
            case ArrowType.NormalArrow:
                //Debug.Log("Set index projectile " + ArrowType.NormalArrow);
                _indexProjectile = (int)ArrowType.NormalArrow;
                break;
            case ArrowType.PiercingArrow:
                //Debug.Log("Set index projectile " + ArrowType.PiercingArrow);
                _indexProjectile = (int)ArrowType.PiercingArrow;
                break;
            case ArrowType.ExplodeArrow:
                //Debug.Log("Set index projectile " + ArrowType.ExplodeArrow);
                _indexProjectile = (int)ArrowType.ExplodeArrow;
                break;
            default:
                break;
        }
    }
    public virtual void IsShooting()
    {
        if (_attackCountdown <= 0)
        {
            GameObject newProjectile = PoolerObjectAdvanced.GetObject(_projectilePrefab[_indexProjectile]);
            newProjectile.transform.forward = _viewPoint.transform.forward;
            newProjectile.transform.position = _shootPoint.transform.position;
            newProjectile.SetActive(true);

            switch (_indexProjectile)
            {
                case (int)ArrowType.PiercingArrow:
                    Debug.Log("Shooted " + ArrowType.PiercingArrow);
                    ShootedPiercingArrow?.Invoke();
                    break;
                case (int)ArrowType.ExplodeArrow:
                    Debug.Log("Shooted " + ArrowType.ExplodeArrow);
                    break;
                default:
                    break;
            }

            _attackCountdown = _attacker.AttackReloadDuration;
            
            StartCoroutine(CountToShoot());

            IEnumerator CountToShoot()
            {
                yield return new WaitForSeconds(0.1f);
                newProjectile.GetComponent<ProjectileBehaviour>().AddForce();
            }
        }
    }
}