using UnityEngine;
using UnityEngine.Events;

public class UIGameOver : MonoBehaviour
{
    public event UnityAction RestartButtonAction;
    public event UnityAction ShopButtonAction;
    public event UnityAction MainMenuAction;

    public void RestartButton()
    {
        RestartButtonAction?.Invoke();
    }

    public void ShopButton()
    {
        ShopButtonAction?.Invoke();
    }
    public void MainMenuButton()
    {
        MainMenuAction?.Invoke();
    }
}
