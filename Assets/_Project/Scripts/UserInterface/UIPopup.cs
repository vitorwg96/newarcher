using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.UI;

public enum PopupButtonType
{
    Confirm,
    Cancel,
}

public enum PopupType
{
    Quit,
    BackToMenu,
}

public class UIPopup : MonoBehaviour
{
    [SerializeField] private LocalizeStringEvent _titleText;
    [SerializeField] private LocalizeStringEvent _descriptionText;
    [SerializeField] private Button _popupButton1;
    [SerializeField] private Button _popupButton2;
    [SerializeField] private InputReader _inputReader;

    private PopupType _actualType;

    public void SetPopup(PopupType popupType)
    {
        _actualType = popupType;
        _titleText.StringReference.TableEntryReference = _actualType.ToString() + "_Popup_Title";
        _descriptionText.StringReference.TableEntryReference = _actualType.ToString() + "_Popup_Description";
        switch (_actualType)
        {
            case PopupType.Quit:
                Debug.Log("IsCaseHere");
                break;
            default:
                break;
        }
    }
}
