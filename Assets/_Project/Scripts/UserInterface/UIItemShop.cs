using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class UIItemShop : MonoBehaviour
{
    public event UnityAction<int, ItemShopType> DecrieseButtonAction;
    public event UnityAction<int, ItemShopType> IncrieseButtonAction;

    [SerializeField] private ItemShopConfigSO _item;
    [SerializeField] private TextMeshProUGUI _amount;
    [SerializeField] private TextMeshProUGUI _buyPrice;
    [SerializeField] private TextMeshProUGUI _nameText;
    [SerializeField] private Image _image;
    [SerializeField] private InventorySO _inventory;

    public void SetTextAmount(string text)
    {
        _amount.text = text;
    }

    public void DecrieseButton()
    {
        DecrieseButtonAction?.Invoke(_item.SellPrice, _item.Type);
    }

    public void IncrieseButton()
    {
        IncrieseButtonAction?.Invoke(_item.BuyPrice, _item.Type);
    }

    private void Start()
    {
        InitializationInformationPanel();
    }

    private void InitializationInformationPanel()
    {
        switch (_item.Type)
        {
            case ItemShopType.NormalArrow:
                _amount.text = _inventory.NormalArrowAmount.ToString();
                break;
            case ItemShopType.PiercingArrow:
                _amount.text = _inventory.PiercingArrowAmount.ToString();
                break;
            case ItemShopType.ExplodeArrow:
                _amount.text = _inventory.ExplodeArrowAmount.ToString();
                break;
            case ItemShopType.HealthPotion:
                _amount.text = _inventory.HealthPotionAmount.ToString();
                break;
            default:
                break;
        }
        //_amount.text = _inventory.PiercingArrowAmount.ToString();
        _buyPrice.text = _item.BuyPrice.ToString();
        _nameText.text = _item.Type.ToString();
        _image.sprite = _item.Icon;
    }
}
