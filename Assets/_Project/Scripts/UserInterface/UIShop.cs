using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class UIShop : MonoBehaviour
{
    public event UnityAction ContinueButtonAction;

    // For a expansive shopping need a List or Dictionary for each item
    [SerializeField] private List<UIItemShop> _scriptItemList = new List<UIItemShop>();
    [SerializeField] private UIItemShop _piercingArrow;
    [SerializeField] private InventorySO _inventory;
    [SerializeField] private TextMeshProUGUI _curretCoin;

    private Dictionary<ItemShopType, UIItemShop> _itemScript;
    private int _unityAmount = 1;

    public void ContinueButton()
    {
        ContinueButtonAction?.Invoke();
    }

    private void Awake()
    {
        _itemScript = new Dictionary<ItemShopType, UIItemShop>();
    }

    private void Start()
    {
        _curretCoin.text = _inventory.CoinAmount.ToString();
        AddDictionaryStart();
    }

    private void OnEnable()
    {
        SubscribeEvent();
    }
    private void OnDisable()
    {
        UnsubscribeEvent();
    }

    private void SellItem(int sellPrice, ItemShopType itemType)
    {
        if (_itemScript.TryGetValue(itemType, out UIItemShop scriptItemShop))
        {
            if (_inventory.GetItemAmountType(itemType) > 0)
            {
                _inventory.SetItemAmountType(_unityAmount, false, itemType);
                _inventory.CoinAmount += sellPrice;
                scriptItemShop.SetTextAmount(_inventory.GetItemAmountType(itemType).ToString());
                _curretCoin.text = _inventory.CoinAmount.ToString();
            }
            else
            {
                Debug.Log("Don't have enough " + itemType);
            }
        }
    }

    private void BuyItem(int buyPrice, ItemShopType itemType)
    {
        if (_itemScript.TryGetValue(itemType, out UIItemShop scriptItemShop))
        {
            if (_inventory.CoinAmount > buyPrice)
            {
                _inventory.SetItemAmountType(_unityAmount, true, itemType);
                _inventory.CoinAmount -= buyPrice;
                scriptItemShop.SetTextAmount(_inventory.GetItemAmountType(itemType).ToString());
                _curretCoin.text = _inventory.CoinAmount.ToString();
            }
            else
            {
                Debug.Log("Don't have enough " + itemType);
            }
        }
    }

    private void AddDictionaryStart()
    {
        _itemScript.Add(ItemShopType.PiercingArrow, _scriptItemList[0]);
    }
    private void SubscribeEvent()
    {
        _scriptItemList[0].DecrieseButtonAction += SellItem;
        _scriptItemList[0].IncrieseButtonAction += BuyItem;
    }
    
    private void UnsubscribeEvent()
    {
        _scriptItemList[0].DecrieseButtonAction -= SellItem;
        _scriptItemList[0].IncrieseButtonAction -= BuyItem;
    }
}
