using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class UIGameInformation : MonoBehaviour
{
    //Enemy Information
    [SerializeField] private EnemySpawn _enemySpawn;
    [SerializeField] private TextMeshProUGUI _enemyCount;
    [SerializeField] private GameObject _wavePanel;
    [SerializeField] private TextMeshProUGUI _waveCount;

    //Player Information
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private TextMeshProUGUI _playerLife;
    [SerializeField] private InventorySO _inventory;
    [SerializeField] private TextMeshProUGUI _coinCount;
    [SerializeField] private SystemProjectileShoot _systemProjectileShoot;
    [SerializeField] private TextMeshProUGUI _arrowCount;

    //Amount that the enemy drops coins
    private int _coinAmountEnemyDrop = 1;

    private void Awake()
    {
        StartInformationPanel();
    }

    private void OnEnable()
    {
        SubscribeEvent();
    }

    private void OnDisable()
    {
        UnsubscribeEvent();
    }

    private void CountShowPiercingArrow()
    {
        Debug.Log("Amount of ArrowAmount: " + _inventory.NormalArrowAmount);
        _inventory.PiercingArrowAmount -= 1;
        _arrowCount.text = _inventory.PiercingArrowAmount.ToString();
    }

    private void ShowCoinInformationCount()
    {
        _inventory.CoinAmount += _coinAmountEnemyDrop;
        _coinCount.text = _inventory.CoinAmount.ToString();
    }

    private void CountPlayerLife(int currentPlayerLife)
    {
        _playerLife.text = currentPlayerLife.ToString();
    }

    private void PopUpNextWave(int waveCount)
    {
        Debug.Log("WaveCount: " + waveCount);
        StartCoroutine(PrintAndWait(3.0f));

        IEnumerator PrintAndWait(float waitTime)
        {
            _waveCount.text = waveCount.ToString();
            _wavePanel.gameObject.SetActive(true);

            yield return new WaitForSeconds(waitTime);
            _wavePanel.gameObject.SetActive(false);
        }
    }

    private void ShowEnemyInformationCount(int enemyCount)
    {
        _enemyCount.text = enemyCount.ToString();
    }

    private void StartInformationPanel()
    {
        _playerLife.text = _playerStats.GetStats().Health.ToString();
        _coinCount.text = _inventory.CoinAmount.ToString();
        _arrowCount.text = _inventory.PiercingArrowAmount.ToString();
        _enemyCount.text = _enemySpawn.EnemyCount.ToString();
    }

    private void SubscribeEvent()
    {
        _enemySpawn.EnemyCountEvent += ShowEnemyInformationCount;
        _enemySpawn.WaveCountEvent += PopUpNextWave;

        _playerStats.TookDamage += CountPlayerLife;

        EnemyStats.IsDead += ShowCoinInformationCount;

        _systemProjectileShoot.ShootedPiercingArrow += CountShowPiercingArrow;
    }

    private void UnsubscribeEvent()
    {
        _enemySpawn.EnemyCountEvent -= ShowEnemyInformationCount;
        _enemySpawn.WaveCountEvent -= PopUpNextWave;

        _playerStats.TookDamage -= CountPlayerLife;
    }
}