using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIGamePlayManager : MonoBehaviour
{
    [SerializeField] private SaveManager _saveManager;
    [SerializeField] private UIGameOver _gameOverPanel;
    [SerializeField] private UIPause _pausePanel;
    [SerializeField] private InputReader _inputReader;
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private UIShop _shopPanel;

    private void Awake()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void OnEnable()
    {
        SubscribeEvent();
    }
    private void OnDisable()
    {
        UnsubscribeEvent();
    }

    private void Update()
    {
        //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int)SceneIndexes.MAIN));
        //Debug.Log("Active Scene : " + SceneManager.GetActiveScene().name);
    }


    private void ShowGameOverMenu()
    {
        Time.timeScale = 0;
        _gameOverPanel.gameObject.SetActive(true);
        Cursor.visible = enabled;
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void ShowPauseMenu()
    {
        Time.timeScale = 0;
        _pausePanel.gameObject.SetActive(true);
        Cursor.visible = enabled;
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void ClosePausePanel()
    {
        Time.timeScale = 1;
        _pausePanel.gameObject.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void ExitGame()
    {
        _saveManager.Save();
        Application.Quit();
    }

    private void ShowShopPanel()
    {
        _shopPanel.gameObject.SetActive(true);
    }
    private void CloseShopPanel()
    {
        _shopPanel.gameObject.SetActive(false);
    }

    private void LoadGamePlayScene()
    {
        GameManager.instance.UnloadScene(SceneIndexes.MAIN);
        GameManager.instance.LoadScene(SceneIndexes.MAIN);
        Time.timeScale = 1;
    }

    private void LoadMainMenuScene()
    {
        GameManager.instance.UnloadScene(SceneIndexes.MAIN);
        GameManager.instance.LoadScene(SceneIndexes.MAIN_MENU);
    }

    private void SubscribeEvent()
    {
        _gameOverPanel.MainMenuAction += LoadMainMenuScene;
        _gameOverPanel.RestartButtonAction += LoadGamePlayScene;
        _gameOverPanel.ShopButtonAction += ShowShopPanel;

        _pausePanel.ExitGameButtonAction += ExitGame;
        _pausePanel.MainMenuButtonAction += LoadMainMenuScene;
        _pausePanel.ResumeButtonAction += ClosePausePanel;

        _inputReader.MenuPauseEvent += ShowPauseMenu;
        _playerStats.IsDead += ShowGameOverMenu;

        _shopPanel.ContinueButtonAction += CloseShopPanel;
    }


    private void UnsubscribeEvent()
    {
        _gameOverPanel.MainMenuAction -= LoadMainMenuScene;
        _gameOverPanel.RestartButtonAction -= LoadGamePlayScene;
        _gameOverPanel.ShopButtonAction -= ShowShopPanel;

        _pausePanel.ExitGameButtonAction -= ExitGame;
        _pausePanel.MainMenuButtonAction -= LoadMainMenuScene;
        _pausePanel.ResumeButtonAction -= ClosePausePanel;

        _inputReader.MenuPauseEvent -= ShowPauseMenu;
        _playerStats.IsDead -= ShowGameOverMenu;

        _shopPanel.ContinueButtonAction -= CloseShopPanel;
    }

}
