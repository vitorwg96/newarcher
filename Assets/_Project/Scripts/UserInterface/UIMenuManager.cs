using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMenuManager : MonoBehaviour
{
    [SerializeField] private SaveManager _saveManager;
	[SerializeField] private UIMainMenu _mainMenuPanel;
    [SerializeField] private UIShop _shopPanel;
    [SerializeField] private UIPopup _popupPanel;

    private void OnEnable()
    {
        SubscribeEvent();
    }

    private void OnDisable()
    {
        UnsubscribeEvent();
    }

    private void StartGame()
    {
        //GameManager.instance.LoadGame();
        GameManager.instance.UnloadScene(SceneIndexes.MAIN_MENU);
        GameManager.instance.LoadScene(SceneIndexes.MAIN);
        _saveManager.Load();
    }
    private void ShowShopPanel()
    {
        Debug.Log("ShowShopPanel");
        _shopPanel.gameObject.SetActive(true);
    }

    private void CloseShopPanel()
    {
        _shopPanel.gameObject.SetActive(false);
    }

    private void ExitGame()
    {
        _saveManager.Save();
        Application.Quit();
    }

    private void SubscribeEvent()
    {
        _mainMenuPanel.StartGameButtonAction += StartGame;
        _mainMenuPanel.ShopButtonAction += ShowShopPanel;
        _mainMenuPanel.ExitButtonAction += ExitGame;

        _shopPanel.ContinueButtonAction += CloseShopPanel;
    }



    private void UnsubscribeEvent()
    {
        _mainMenuPanel.StartGameButtonAction -= StartGame;
        _mainMenuPanel.ShopButtonAction -= ShowShopPanel;
        _mainMenuPanel.ExitButtonAction -= ExitGame;
        
        _shopPanel.ContinueButtonAction -= CloseShopPanel;
    }
}