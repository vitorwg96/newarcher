using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;


public class UIGenericButton : MonoBehaviour
{
	[SerializeField] private LocalizeStringEvent _buttonText = default;
	[SerializeField] private Button _button = default;


	private bool _isDefaultSelection = false;

	private void OnDisable()
	{
		_isDefaultSelection = false;
	}

	public void SetButton(LocalizedString localizedString, bool isSelected)
	{
		_buttonText.StringReference = localizedString;

		if (isSelected)
			SelectButton();
	}

	public void SetButton(string tableEntryReference, bool isSelected)
	{
		_buttonText.StringReference.TableEntryReference = tableEntryReference;

		if (isSelected)
			SelectButton();
	}

	public void SelectButton()
	{
		_button.Select();
	}
}
