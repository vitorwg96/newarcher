using UnityEngine;
using UnityEngine.Events;

public class UIMainMenu : MonoBehaviour
{
	public event UnityAction StartGameButtonAction;
	public event UnityAction ShopButtonAction;
	public event UnityAction SettingsButtonAction;
	public event UnityAction ExitButtonAction;

	public void StartGameButton()
	{
		StartGameButtonAction?.Invoke();
	}

	public void ShopButton()
	{
		ShopButtonAction?.Invoke();
	}

	public void SettingsButton()
	{
		SettingsButtonAction?.Invoke();
	}

	public void ExitButton()
	{
		ExitButtonAction?.Invoke();
	}
}
