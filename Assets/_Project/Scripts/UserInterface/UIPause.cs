using UnityEngine;
using UnityEngine.Events;

public class UIPause : MonoBehaviour
{
    public event UnityAction ResumeButtonAction;
    public event UnityAction MainMenuButtonAction;
    public event UnityAction ExitGameButtonAction;

    public void ResumeButton()
    {
        ResumeButtonAction?.Invoke();
    }

    public void MainMenuButton()
    {
        MainMenuButtonAction?.Invoke();
    }

    public void ExitGameButton()
    {
        ExitGameButtonAction?.Invoke();
    }
}
